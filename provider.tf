terraform {
  required_version = ">= 1.2"
  backend "s3" {
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.14"
    }
    local = {
      version = "~>2.2"
    }

  }
}

# Set the AWS provider with the desired region
provider "aws" {
  region = var.aws_region
}