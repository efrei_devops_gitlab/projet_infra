########################
# Network
########################
# Create the VPC
#tfsec:ignore:aws-ec2-require-vpc-flow-logs-for-all-vpcs
resource "aws_vpc" "web" {
  cidr_block = var.vpc_cidr
  tags = {
    Name = "WebVpc"
  }
}
# Create the public subnets
resource "aws_subnet" "public_subnets" {
  count                   = length(var.availability_zones)
  vpc_id                  = aws_vpc.web.id
  cidr_block              = var.public_subnet_cidrs[count.index]
  availability_zone       = var.availability_zones[count.index]
  map_public_ip_on_launch = true #tfsec:ignore:aws-ec2-no-public-ip-subnet
  tags = {
    Name = "WebPublic-${count.index}"
  }
}

# Create the private subnets
resource "aws_subnet" "private_subnets" {
  count             = length(var.availability_zones)
  vpc_id            = aws_vpc.web.id
  cidr_block        = var.private_subnet_cidrs[count.index]
  availability_zone = var.availability_zones[count.index]
  tags = {
    Name = "WebPrivate-${count.index}"
  }
}

# Create an internet gateway
resource "aws_internet_gateway" "web_igw" {
  vpc_id = aws_vpc.web.id
  tags = {
    Name = "Web_GW"
  }
}

# Create a route table for the public subnets
resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.web.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.web_igw.id
  }
  tags = {
    Name = "Web_Public"
  }
}

resource "aws_route_table_association" "public" {
  count          = length(aws_subnet.public_subnets)
  subnet_id      = aws_subnet.public_subnets[count.index].id
  route_table_id = aws_route_table.public_route_table.id
}

resource "aws_security_group" "public" {
  description = "Allow all outbound and inbound traffic from and to public"
  name_prefix = "public-"
  vpc_id      = aws_vpc.web.id
  ingress {
    description = "Allow Apache access from public"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] #tfsec:ignore:aws-ec2-no-public-ingress-sgr
  }
  egress {
    description = "Allow all outbound traffic to public"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"] #tfsec:ignore:aws-ec2-no-public-egress-sgr
  }
  tags = {
    Name = "Public"
  }
}

resource "aws_security_group" "private" {
  description = "Allow all outbound and inbound traffic from public subnets"
  name_prefix = "private-"
  vpc_id      = aws_vpc.web.id
  ingress {
    description = "Allow Apache access from public subnets"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = aws_subnet.public_subnets[*].cidr_block
  }
  egress {
    description = "Allow all outbound traffic to public subnets"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = aws_subnet.public_subnets[*].cidr_block
  }
  tags = {
    Name = "Private"
  }
}